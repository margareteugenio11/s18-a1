console.log("Hello world");

function trainer(name, age, numBadge){
	this.name = name;
	this.age = age;
	this.numBadge = numBadge;
};

let Margaret = new trainer("Margaret", 23, "4 Badges");
Margaret.numPokemon = "Margaret has caught 57 pokemon"
console.log(Margaret);

function Pokemon(name, health){
	this.name = name;
	this.health = health;
	this.attack = function(target, move){
		console.log(`${this.name} used ${move} to ${target.name}. Fainted!`);
		target.health -= 100;
		console.log(`${target.name}'s health is now ${target.health}`)
	}
};

let Jigglypuff = new Pokemon("Jigglypuff", 750);
let Togepi = new Pokemon("Togepi", 750);
let Eevee = new Pokemon("Eevee", 840);

Jigglypuff.attack(Togepi, "Psychic");
Eevee.attack(Jigglypuff, "Quick attach");
Togepi.attack(Eevee, "Tail whip");
